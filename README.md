## Tutorial

From [here](https://techvblogs.com/blog/laravel-9-crud-application-tutorial-with-example).

## Stack

Laravel Framework 10.x-dev

PHP 8.2.0

Bootstrap 4.5

## License

Licensed under the [MIT license](https://opensource.org/licenses/MIT).
